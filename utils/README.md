# Basic Utilities

Provides basic utilities to facilitate more advantaged use cases.

To be included here the code needs to require following conditions:
* C++ with STL only
* No references to particular window systems.
* MIT licensed
